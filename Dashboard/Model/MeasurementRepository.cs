﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dashboard.Model
{
    public class MeasurementRepository : IMeasurementRepository
    {
        private readonly MeasurementContext _context;

        public MeasurementRepository(MeasurementContext context)
        {
            _context = context;
        }

        public MeasurementItem GetOne()
        {
            return _context.Measurements.FirstOrDefault();
        }

        public IEnumerable<MeasurementItem> Get(int n)
        {
            return _context.Measurements.Take(n).ToList();
        }
    }
}
