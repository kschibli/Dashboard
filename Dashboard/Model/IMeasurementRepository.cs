﻿using System.Collections;
using System.Collections.Generic;

namespace Dashboard.Model
{
    public interface IMeasurementRepository
    {
        MeasurementItem GetOne();
        IEnumerable<MeasurementItem> Get(int n);
    }
}