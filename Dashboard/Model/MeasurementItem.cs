﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Dashboard.Model
{
    public class MeasurementItem
    {
        [Key]
        public Guid Id { get; set; }
        public double Temperature { get; set; }
        public double Time { get; set; }
        public double Altitude { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
    }
}
