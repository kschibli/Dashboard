﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dashboard.Model;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Dashboard.Controllers
{
    [Route("api/[controller]")]
    public class MeasurementController : Controller
    {
        private readonly IMeasurementRepository _measurementRepository;

        public MeasurementController(IMeasurementRepository measurementRepository)
        {
            _measurementRepository = measurementRepository;
        }

        // GET: api/values
        [HttpGet]
        public JsonResult Get()
        {
            return Json(_measurementRepository.GetOne());
        }

        // GET api/values/5
        [HttpGet("{count}")]
        public JsonResult Get(int count)
        {
            return Json(_measurementRepository.Get(count));
        }
    }
}
